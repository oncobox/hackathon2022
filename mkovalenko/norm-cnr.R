normalizeExpr <- function(E) {
  library('DESeq2')
  library('psych')
  library('preprocessCore')
  size.factors <- estimateSizeFactorsForMatrix(as.matrix(E+1))
  m3 <- t(t(as.matrix(E+1)) / size.factors)
  dimnames(m3) <- dimnames(E)
  return(m3)
}

calcCNR <- function(m3, norms) {
  # Case-to-normal ratios
  library('psych')
  gmeans <- geometric.mean(t(as.matrix(norms)))
  if(length(gmeans) == nrow(m3)) {
    CNR <- m3 / gmeans
    return(CNR)
  } else {
    message('Error in calcCNR()')
    return()
  }
}


# Reading the data
message('Reading data...')
library('openxlsx')
in.dir <- file.path('data', 'raw_hgnc_counts')
norm <- list()
tum <- list()
count <- 0
for(fname in list.files(in.dir, full.names = FALSE)) {
  file <- file.path(in.dir, fname)
  id <- gsub('^raw_hgnc_counts,CPTAC_', '', fname)
  id <- gsub('.xlsx$', '', id)
  id <- gsub('_CPTAC$', '', id)
  # read norms
  norm1 <- read.xlsx(file, sheet = 'norm')
  norm[[id]] <- as.matrix(norm1[,-1])
  rownames(norm[[id]]) <- norm1[,1]
  # read tums
  tum1 <- read.xlsx(file, sheet = 'tum')
  tum[[id]] <- as.matrix(tum1[,-1])
  rownames(tum[[id]]) <- tum1[,1]
  # check gene names
  count <- count + 1
  if(count == 1) {
    genes <- norm1[,1]
  }
  f1 <- all(norm1[,1] == genes)
  f2 <- all(tum1[,1] == genes)
  if(!(f1 & f2)) {
    message('Warning: different gene names at ', id)
  }
}
# Check if all case ids are unique
cases.tum <- unlist(lapply(tum, function(x) {return(colnames(x))}))
cases.norm <- unlist(lapply(norm, function(x) {return(colnames(x))}))
cases <- c(cases.tum, cases.norm)
if(length(cases) != length(unique(cases))) {
  message('Warning: duplicate case ids')
}
cases.common <- intersect(cases.tum, cases.norm)
if(length(cases.common) > 0) {
  message('Warning: common case ids in tumors and norms')
}

# Combine all tums and all norms into a single matrix
message('Merging data...')
total <- tum[[1]]
for(id in names(tum)[-1]) {
  total <- cbind(total, tum[[id]])
}
for(id in names(norm)) {
  total <- cbind(total, norm[[id]])
}


# Normalize it all
message('Normalizing...')
total <- normalizeExpr(total)

# Split it back into tums and norms
m3.tum.all <- total[,cases.tum]
m3.norm.all <- total[,cases.norm]

# Calculate CNRs
message('Calculating CNRs...')
CNR <- list()
for(id in names(tum)) {
  m3.tum <- m3.tum.all[,colnames(tum[[id]])]
  m3.norm <- m3.norm.all[,colnames(norm[[id]])]
  if(ncol(m3.norm) == 0) {
    message('No normal controls for ', id, ' skipping it')
  } else {
    CNR[[id]] <- calcCNR(m3.tum, m3.norm)
  }
}
CNR.all <- CNR[[1]]
for(id in names(CNR)[-1]) {
  if(any(rownames(CNR[[id]]) != rownames(CNR.all))) {
    message('Warning: different CNR colnames in ', id)
    next
  }
  CNR.all <- cbind(CNR.all, CNR[[id]])
}


# Output
# This ate all my memory and killed my entire family, help
if(FALSE) {
  outfile <- file.path('raw_hgnc_counts', 'CNR_CPTAC.xlsx')
  wb <- createWorkbook()
  addWorksheet(wb, 'CNR_CPTAC')
  writeData(wb, 'CNR_CPTAC', CNR.all)
  saveWorkbook(wb, outfile, overwrite = TRUE)
}

message('Writing output...')
library(data.table)
fwrite(cbind(data.table(V1 = rownames(CNR.all)), as.data.table(CNR.all)),
       file = file.path('raw_hgnc_counts', 'CNR_CPTAC.csv'),
       quote = FALSE, row.names = FALSE)
message('')
